# Wheatstone Bridge Amplifier

This is an amplifier, which evaluates a signal from a strain gage during battery operation and displays the resulting force. Particular emphasis is placed on small overall dimensions, low energy consumption and low component costs. Furthermore, an acoustic warning signal, which signals the exceeding of an adjustable maximum tractive force, should be implemented.

See CircuitStudio Project: https://circuitmaker.com/Projects/Details/Andreas-Hirtenlehner/BridgeAmplifier

## Design

The HMI (Human Machine Interface) consists of:
  - a three-digit 7-segment display,
  - an on / off switch,
  - two buttons for setting the maximum force and
  - a button for displaying the battery level
  
  The dimensions of the board are 60mm x 45mm with all controls on the top. To charge the battery, a micro USB socket is provided.
  
## Sepcification

  - Battery life: 4h
  - Maximum input voltage: 2mV 
  - Bridge resistance: 120Ω
  - Bridging connection: 4-wire 
  
  To compensate for any offset errors, the force is set to 0N at switch-on.

![alt text](09_Pictures/bridge_amp.jpg "Bridge Amplifier")