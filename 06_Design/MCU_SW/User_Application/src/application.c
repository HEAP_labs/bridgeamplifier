/**
  * @file    application.c 
  * @author  Andreas Hirtenlehner
  * @brief   Implementation of the application code
  */

/* Includes ------------------------------------------------------------------*/
#include "application.h"
#include "arm_math.h"

/** @addtogroup Application
  * @{
  */

/** @defgroup Tasks 
  * @brief realtime tasks
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
q15_t adc_data[6];

double battery_charge_lt[] = 
{
	3.0,
	3.1,
	3.2,
	3.3,
	3.4,
	3.5,
	3.6,
	3.7,
	3.8
};

/* Private function prototypes -----------------------------------------------*/
void taskclass1(void);
void taskclass1_shifted(void);
void taskclass2(void);
void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Tasks_Private_Functions
  * @{
  */

/**
  * @brief  fastest taskclass with highest priority (called periodically)
  */
void taskclass1()
{
#ifdef __USING_7S
	LIB_7S_digit_increment();
#endif // __USING_7S

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  fastest taskclass with highest priority (called periodically after taskclass1)
  */
void taskclass1_shifted()
{	
#ifdef __USING_ADC
	API_ADC_DMA_start_single_conv(&API_ADC1);
#endif // __USING_ADC

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  taskclass (called periodically)
  */
void taskclass2()
{
  static uint8_t iniOK = 0;
	uint8_t        i;
	
	static uint8_t startup;
	static uint8_t m_startup;
	uint8_t        alarm;
	static uint8_t m_alarm;
	
	static double t_show_s;
	static double t_up_s;
	static double t_on_s;
	
	static parameters_t param;
	static double       force_limit_kN;
	static double       u_offset_rel;
	uint8_t             battery_charge;
	
  static LIB_hw_signal_t hw_signal_power    = { POWER_PP   };
  static LIB_hw_signal_t hw_signal_battery  = { BATTERY_PP };
  static LIB_hw_signal_t hw_signal_up       = { UP_PP      };
  static LIB_hw_signal_t hw_signal_down     = { DOWN_PP    };
	
	const double u_amplifier_rel = adc_data[AMPLIFIER_ADC_CH] / 4095.0;
	const double u_reference_rel = adc_data[REFERENCE_ADC_CH] / 4095.0;
	const double u_battery_v     = adc_data[BATTERY_ADC_CH] * g_adc_ref_v / 4095.0 * 25.0 / 15.0;
	const double force_kN        = (COMMON_CORR * SHAFT_AREA * SIGMA_REL / AMPLIFIER_GAIN / 1000.0) * (u_amplifier_rel - u_reference_rel - u_offset_rel);
	
	/* read buttons */
	LIB_hw_signal_read(&hw_signal_power,   T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_battery, T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_up,      T_PERIOD2_S);
	LIB_hw_signal_read(&hw_signal_down,    T_PERIOD2_S);
	
	/* power down */
	if(!iniOK);
	else if(hw_signal_power.o_pos_edge && !startup) { API_getDI(LDO_ENABLE_PP); } // set pin to high impedance
	else if(t_up_s > MAX_UP_TIME_S)								  { API_getDI(LDO_ENABLE_PP); } 
	else if(u_battery_v < 3.0) 											{ API_getDI(LDO_ENABLE_PP); }
	// else;
	
	/* battery_charge */
	for(i = 0; i < sizeof(battery_charge_lt)/8; i++)
	{
		if(battery_charge_lt[i] >= u_battery_v)     { battery_charge = (i+1)*10; break; }
		else if(i >= sizeof(battery_charge_lt)/8-1) { battery_charge = 100;             }
		// else;
	}

	/* parameter to show */
	if(!iniOK)                            { param = FORCE;           }
	else if(hw_signal_battery.o_neg_edge) { param = BATTERY_CHARGE;  }
	else if(hw_signal_battery.o_pos_edge) { param = BATTERY_VOLTAGE; } // TEST
	else if(hw_signal_up.o_neg_edge)      { param = FORCE_LIMIT;     }
	else if(hw_signal_down.o_neg_edge)    { param = FORCE_LIMIT;     }
	else if(t_show_s > PARAM_SHOW_TIME_S) { param = FORCE;           }
	// else;
	
	/* max force config */
	if(!iniOK) 												 																	    			  								 { force_limit_kN = DEF_FORCE_LIMIT_KN;                   }
	else if(hw_signal_up.o_neg_edge && force_limit_kN + FORCE_LIMIT_STEP_KN <= FORCE_RANGE_LIMIT_KN) { force_limit_kN = force_limit_kN + FORCE_LIMIT_STEP_KN; }
	else if(hw_signal_down.o_neg_edge && force_limit_kN - FORCE_LIMIT_STEP_KN > 0)  								 { force_limit_kN = force_limit_kN - FORCE_LIMIT_STEP_KN; }
	// else;
	
	/* buzzer */
	if(!iniOK)							           { alarm = 0; }
	else if(force_kN > force_limit_kN) { alarm = 1; }
	else 											         { alarm = 0; }
	
	if(!iniOK);
	else if(alarm && !m_alarm) { API_PWM_start(&API_TIM17, CH1, BUZZER_PP, 1.0/2700.0, 0.5, 0, TIM_OCPolarity_High); }
	else if(!alarm && m_alarm) { API_TIM_stop(&API_TIM17, TIM_IT_Update);	API_setDO(BUZZER_PP, Bit_RESET);		       }
	// else;
	
	if(!iniOK) 											 { startup = 1; }
	else if(startup && t_on_s < 0.3) { startup = 0; }
	// else;
	
	/* set offset voltage after startup */
	if(!iniOK) 										 { u_offset_rel = 0;                                 }
	else if(m_startup && !startup) { u_offset_rel = u_amplifier_rel - u_reference_rel; }
	// else;
	
	/* time to show a parameter */
	if(!iniOK) 		                          { t_show_s = 0; 										 }
	else if(!hw_signal_battery.o_debounced) { t_show_s = 0; 										 }
	else if(!hw_signal_up.o_debounced)      { t_show_s = 0; 										 }
	else if(!hw_signal_down.o_debounced)    { t_show_s = 0; 										 }
	else if(param != FORCE)                 { t_show_s = t_show_s + T_PERIOD2_S; }
	else 					                          { t_show_s = 0; 										 }
	
	if(!iniOK)       { t_on_s = 0;                    }
	else if(startup) { t_on_s = t_on_s + T_PERIOD2_S; }
	// else;
	
	if(!iniOK)                                { t_up_s = 0; 									 }
	else if(!hw_signal_battery.m_o_debounced) { t_up_s = 0; 									 }
	else if(!hw_signal_up.m_o_debounced)      { t_up_s = 0; 									 }
	else if(!hw_signal_down.m_o_debounced)    { t_up_s = 0; 									 }
	else                                      { t_up_s = t_up_s + T_PERIOD2_S; }
	// else;
	
	/* seven segment output */
	if(param == FORCE)                { LIB_7S_set_format(1, 1, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(force_kN);       }
	else if(param == BATTERY_CHARGE) 	{	LIB_7S_set_format(2, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(battery_charge); }
	else if(param == BATTERY_VOLTAGE) { LIB_7S_set_format(1, 2, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(u_battery_v);    }
	else if(param == FORCE_LIMIT)     { LIB_7S_set_format(1, 0, RIGHT, 0); LIB_7S_SPI_DMA_TIM_show_number(force_limit_kN); }
	else 															{ LIB_7S_SPI_clear();                             																	 }
	
	m_startup = startup;
	m_alarm   = alarm;
	
	iniOK = 1;
  
#ifdef __USING_IWDG
  IWDG_ReloadCounter();
#endif // __USING_IWDG
}

/**
  * @brief  slowest taskclass (soft-realtime; called periodically in taskclass2)
  */
void soft_rt_taskclass()
{

}

/**
  * @brief startup - function, called in the main module
*/
void startup_sequence(void)
{
  SystemCoreClockUpdate();
	
	/* hold LDO enable */
	API_setDO(LDO_ENABLE_PP, Bit_SET);
	
	/* enable battery measurement */
	API_setDO(BATTERY_MEASURE_PP, Bit_RESET);
	
	/* set NTC input to high impedance */
	API_getDI(NTC_ADC_PP);

#ifdef __USING_ADC
  g_adc_ref_v = 3.0;
	API_ADC_DMA_init(	&API_ADC1, 
										(uint16_t*)adc_data, 
										ADC_SampleTime_239_5Cycles, 
										3, 
										BATTERY_ADC_PP,
										AMPLIFIER_ADC_PP, 
										REFERENCE_ADC_PP );
#endif // __USING_ADC
	
#ifdef __USING_7S
	LIB_7S_set_display_assembly(3, COMMON_ANODE);
	LIB_7S_set_segment_bit_position(3, 4, 6, 7, 8, 2, 1, 5);
	LIB_7S_set_digit_portpin(3, DIGIT1_PP, DIGIT2_PP, DIGIT3_PP);
	LIB_7S_set_format(1, 1, RIGHT, 0);
	
	#ifdef __USING_SPI
		LIB_7S_SPI_init(&API_SPI1, 
										SPI_MOSI_PP, 
										SPI_CLK_PP, 
										LE_PP, 
										N_OE_PP, 
										SPI_BaudRatePrescaler_128, 
										SPI_MODE_0);
		LIB_7S_SPI_DMA_TIM_start_continious_tx(&API_TIM16, T_PERIOD1_S);
	#endif // __USING_SPI
#endif // __USING_7S

#ifdef __USING_TIMER
  API_TIM_start(&API_TIM16, TIM_IT_CC1, T_PERIOD1_S, 0);
  API_PWM_start(&API_TIM16, CH1, N_OE_PP, T_PERIOD1_S, 0.5, 0xFF, TIM_OCPolarity_High);
  API_TIM_start(&API_TIM14, TIM_IT_Update, T_PERIOD2_S, 5);
#endif // __USING_TIMER
  
#ifdef __USING_IWDG
  API_IWDG_init(); //updated in taskclass2
#endif // __USING_IWDG

#ifdef __USING_WWDG  
  API_WWDG_init(); //updated in taskclass1
#endif // __USING_WWDG
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 
