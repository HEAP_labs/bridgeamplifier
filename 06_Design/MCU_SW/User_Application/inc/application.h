/**
  * @file    application.h
  * @author  Andreas Hirtenlehner
  * @brief   Header file for the application module
  */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup Application
  * @{
  */

/** @addtogroup Tasks 
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  parameters to show on display
  */
	
typedef enum parameters_e
{
	FORCE = 0,
	BATTERY_CHARGE,
	BATTERY_VOLTAGE,
	FORCE_LIMIT,
	TEMPERATURE,
	NTC_VOLTAGE,
	AMPLIFIER_VOLTAGE,
	REFERENCE_VOLTAGE,
	BRIDGE_POS_VOLTAGE,
	BRIDGE_NEG_VOLTAGE,
	CLEAR,
} parameters_t;

/* Exported constants --------------------------------------------------------*/
/** @defgroup Tasks_Exported_Constants
  * @{
  */ 

/**
  * @brief taskclass period time definitions [s]
  * @{
  */

#define T_PERIOD1_S         ((double) 2e-3)
#define T_PERIOD2_S         ((double) 1e-1)
#define T_PERIOD_SOFT_RT_S  ((double) 1.0)
	
/**
  * @}
  */ 
	
/**
  * @brief user definitions
  * @{
  */

#define MAX_UP_TIME_S				((double) 60.0)	
#define PARAM_SHOW_TIME_S		((double) 2.0)
	
#define DEF_FORCE_LIMIT_KN 	((double) 25.0) /* force for alarm */
#define FORCE_RANGE_LIMIT_KN ((double) 30.0) /* measurement range limit */
#define FORCE_LIMIT_STEP_KN ((double) 1.0)
	
/**
  * @}
  */ 

/**
  * @brief portpin definitions
  * @{
  */

#define BATTERY_PP 					((API_GPIO_type_t*) &PC13)
#define UP_PP 							((API_GPIO_type_t*) &PC14)
#define DOWN_PP 						((API_GPIO_type_t*) &PC15)
#define POWER_PP 						((API_GPIO_type_t*) &PF6)

#define LDO_ENABLE_PP 			((API_GPIO_type_t*) &PF7)
#define BATTERY_MEASURE_PP 	((API_GPIO_type_t*) &PA0)

#define LED_PP						  ((API_GPIO_type_t*) &PB8)
#define BUZZER_PP				    ((API_GPIO_type_t*) &PB9)
#define TP7_PP						  ((API_GPIO_type_t*) &PB7)

#define SPI_MOSI_PP 				((API_GPIO_type_t*) &PB5)
#define SPI_MISO_PP 				((API_GPIO_type_t*) &PB4)
#define SPI_CLK_PP 					((API_GPIO_type_t*) &PB3)
#define LE_PP 							((API_GPIO_type_t*) &PA15)
#define N_OE_PP 						((API_GPIO_type_t*) &PB6)

#define DIGIT1_PP 					((API_GPIO_type_t*) &PA12)
#define DIGIT2_PP 					((API_GPIO_type_t*) &PA11)
#define DIGIT3_PP 					((API_GPIO_type_t*) &PA10)

#define AMPLIFIER_ADC_PP    ((API_GPIO_type_t*) &PA2)
#define REFERENCE_ADC_PP    ((API_GPIO_type_t*) &PA3)
#define H_BRIDGE_P_ADC_PP   ((API_GPIO_type_t*) &PA5)
#define H_BRIDGE_N_ADC_PP   ((API_GPIO_type_t*) &PA6)
#define NTC_ADC_PP          ((API_GPIO_type_t*) &PA4)
#define BATTERY_ADC_PP      ((API_GPIO_type_t*) &PA1)

/**
  * @brief ADC Channels
  * @{
  */

#define AMPLIFIER_ADC_CH 		((uint8_t) 1)
#define REFERENCE_ADC_CH 		((uint8_t) 2)
#define H_BRIDGE_P_ADC_CH 	((uint8_t) 4)
#define H_BRIDGE_N_ADC_CH 	((uint8_t) 5)
#define NTC_ADC_CH 					((uint8_t) 3)
#define BATTERY_ADC_CH 			((uint8_t) 0)

/**
  * @}
  */ 
	
/**
  * @brief Amplifier definitions
  * @{
  */
	
#define AAF_RES							((double) 10e3) /* R3A */
#define GAIN_RES						((double)  1e3) /* R8A,  Datasheet: R1 */
#define FEEDBACK_RES				((double)  1e6) /* R9A,  Datasheet: R2 */
#define OFFSET_RES_1				((double)  1e5) /* R11A, Datasheet: R3 */
#define OFFSET_RES_2				((double) 3300) /* R10A, Datasheet: R4 */
#define AMPLIFIER_GAIN 			((double) (1 + (FEEDBACK_RES + OFFSET_RES_1 * OFFSET_RES_2 / (OFFSET_RES_1 + OFFSET_RES_2)) / GAIN_RES))

/**
  * @}
  */ 

/**
  * @brief Mechanical definitions
  * @{
  */

#define GAUGE_FACTOR 				((double) 2.0) /* k-factor */
#define POISSON_RATIO 			((double) 0.3) 
#define YOUNGS_MODULUS 			((double) 180e9)
#define SHAFT_DIAMETER 			((double) 0.013) /* diameter in m */

/**
  * @}
  */ 

/**
  * @brief Mechanical characteristics
  * @{
  */

#define SHAFT_AREA 					((double) (SHAFT_DIAMETER * SHAFT_DIAMETER * PI / 4.0))
#define SIGMA_REL 					((double) (2.0 * YOUNGS_MODULUS / (GAUGE_FACTOR * (1 + POISSON_RATIO)))) /* sigma / (Ud/U) */

/**
  * @}
  */ 

/**
  * @brief Correction factors
  * @{
  */

#define COMMON_CORR 				((double) 1.0)

/**
  * @}
  */ 

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __APPLICATION_H
