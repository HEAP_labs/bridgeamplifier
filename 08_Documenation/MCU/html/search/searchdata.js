var indexSectionsWithContent =
{
  0: "abdeghilmnpstw",
  1: "al",
  2: "aeghlmpstw",
  3: "ahlmnpst",
  4: "p",
  5: "lp",
  6: "lp",
  7: "e",
  8: "aghilmpstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Macros",
  8: "Modules"
};

