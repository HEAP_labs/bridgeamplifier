var struct_a_p_i___t_i_m__type__t =
[
    [ "DMAx_Channelx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a15ccb5efead399be16dcc79e6ef0afb4", null ],
    [ "GPIO_AF_TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a53ab4d29115009c0c013b4bb100c5655", null ],
    [ "IRQnx", "da/de0/struct_a_p_i___t_i_m__type__t.html#aae957cbdbce5c44eac6cf46d4a099064", null ],
    [ "period_timer_ticks", "da/de0/struct_a_p_i___t_i_m__type__t.html#abd297952e4487813eb634ecca54b2b0a", null ],
    [ "RCC_APBxPeriph_TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#abb91f6582261e0f29fc7c7e21fe1e4bc", null ],
    [ "TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a663322e9eb0b01f9723211f3973c31ef", null ]
];