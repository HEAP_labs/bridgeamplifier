var group___s_p_i___private___functions =
[
    [ "API_SPI_DMA_init", "da/dad/group___s_p_i___private___functions.html#ga2d4f089590595dedff78a7637a2eb833", null ],
    [ "API_SPI_DMA_send", "da/dad/group___s_p_i___private___functions.html#ga342c6c4e2360f0c8bb5110ad84949fca", null ],
    [ "API_SPI_DMA_send_and_receive", "da/dad/group___s_p_i___private___functions.html#ga2d3166a379ddc7223382a7649f3fd944", null ],
    [ "API_SPI_DMA_send_then_receive_statemachine", "da/dad/group___s_p_i___private___functions.html#ga1c9b0499d6a75f5fd28b2dbd1254a38b", null ],
    [ "API_SPI_DMA_TIM_start_continious_tx", "da/dad/group___s_p_i___private___functions.html#ga1b24f1c7339cae5d9efcababbe90d281", null ],
    [ "API_SPI_DMA_TIM_stop_continious_tx", "da/dad/group___s_p_i___private___functions.html#ga50dcf46aecfb1f59aaa8e8a72f40adaa", null ],
    [ "API_SPI_init", "da/dad/group___s_p_i___private___functions.html#ga442635c0f487e970ef63ac591aa70fc4", null ],
    [ "API_SPI_receive_byte", "da/dad/group___s_p_i___private___functions.html#gaa5f5283efc84ecd331b455abf8c38e39", null ],
    [ "API_SPI_send_byte", "da/dad/group___s_p_i___private___functions.html#gaee7c655673f111cba8a7e025b806f878", null ],
    [ "API_SPI_send_then_receive_statemachine", "da/dad/group___s_p_i___private___functions.html#gabe73227fde84b91ca8719a73e409b084", null ]
];