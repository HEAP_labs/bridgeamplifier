var group___a_d_c =
[
    [ "ADC_Private_Functions", "d4/d52/group___a_d_c___private___functions.html", "d4/d52/group___a_d_c___private___functions" ],
    [ "API_ADC_type_t", "d1/da0/struct_a_p_i___a_d_c__type__t.html", [
      [ "ADCx", "d1/da0/struct_a_p_i___a_d_c__type__t.html#a171e004c95ad397ecd1d47a173f30979", null ],
      [ "p_adc_data", "d1/da0/struct_a_p_i___a_d_c__type__t.html#a89995bc420830ef01eab9cfa79cf0780", null ],
      [ "RCC_APB2Periph_ADCx", "d1/da0/struct_a_p_i___a_d_c__type__t.html#abd74f519316d03e0c3b912705b9728b5", null ]
    ] ],
    [ "API_ADC_DMA_init", "db/d78/group___a_d_c.html#ga03e894b5911956fc55fc9015d851c10f", null ],
    [ "API_ADC_DMA_start_single_conv", "db/d78/group___a_d_c.html#ga465f3eeb4e57712a9cbfe0502c835587", null ],
    [ "API_ADC_get_mcu_temp_gradc", "db/d78/group___a_d_c.html#gad46253ef4c9386fae8d457183919f04b", null ],
    [ "API_ADC_get_ue_v", "db/d78/group___a_d_c.html#ga170db7b663029dab41425f041bf7cae1", null ],
    [ "API_ADC_init", "db/d78/group___a_d_c.html#ga7f9c0f2529c2c4bc1498c10aca00e7a9", null ]
];