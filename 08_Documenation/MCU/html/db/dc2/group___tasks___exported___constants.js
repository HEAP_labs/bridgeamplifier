var group___tasks___exported___constants =
[
    [ "AMPLIFIER_ADC_CH", "db/dc2/group___tasks___exported___constants.html#ga05593f13d8c3c668a18608332b883d1a", null ],
    [ "AMPLIFIER_GAIN", "db/dc2/group___tasks___exported___constants.html#gaf92ed4044fdaecd437ed070ac7cd2cc1", null ],
    [ "BATTERY_PP", "db/dc2/group___tasks___exported___constants.html#ga5e6f1964d9ce5b34514fe2b8fd5cfbbf", null ],
    [ "DEF_FORCE_LIMIT_KN", "db/dc2/group___tasks___exported___constants.html#ga292408b54a1d4a7cf2c4e12f51b9a421", null ],
    [ "MAX_UP_TIME_S", "db/dc2/group___tasks___exported___constants.html#ga6691838de8e6e46ed6bd709a52a827e8", null ],
    [ "SHAFT_AREA", "db/dc2/group___tasks___exported___constants.html#ga06ea10aa5151ed967cafe7663b63f51f", null ],
    [ "T_PERIOD1_S", "db/dc2/group___tasks___exported___constants.html#gae092471c2ee0f2b1076d1bd988b14cbf", null ]
];