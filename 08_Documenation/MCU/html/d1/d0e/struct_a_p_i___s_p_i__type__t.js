var struct_a_p_i___s_p_i__type__t =
[
    [ "DMA_Channel_x_Rx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a773bfc834c6df3e002cbf1d2e2677421", null ],
    [ "DMA_Channel_x_Tx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#acfaaf898f5c036fb13e011db5c4ceb8c", null ],
    [ "DMA_IRQn", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a67be1b93d535820facbedd66ba44e8b6", null ],
    [ "DMAx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#ab31c445620084a1e99142d5e541246d3", null ],
    [ "GPIO_AF_SPIx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#ae3ef4e7b1937f7c15f77d689f6ca56e0", null ],
    [ "is_init", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a1e54fa0af2d142c1cf2cec156e08e34b", null ],
    [ "miso_portpin", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a524aa303a38e29d317a451d76d416d39", null ],
    [ "mosi_portpin", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#abd222338d06d90935429406e5ee12bac", null ],
    [ "nss_portpin", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a198e1db88a80af02172cb7afbc74f36f", null ],
    [ "p_rx_data", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a7f54c940ae2ad8b6922d2999bb0f70a4", null ],
    [ "p_tx_data", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a89e0c695f2d39172de0a2d9bae78b73f", null ],
    [ "RCC_AHB1Periph_DMAx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#aa526d7e1df5783fa862c64dfd35e19de", null ],
    [ "RCC_APBxPeriph_SPIx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#aebb59f08091422cb8e8421e62e0246a9", null ],
    [ "sck_portpin", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a6a07dd627c0fde2a3ef4be19bff05472", null ],
    [ "SPIx", "d1/d0e/struct_a_p_i___s_p_i__type__t.html#a494795212cc0f4f2ffb02c24b9dc4f0c", null ]
];