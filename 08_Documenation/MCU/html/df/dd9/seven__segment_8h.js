var seven__segment_8h =
[
    [ "LIB_7S_character_t", "d9/d68/group__seven__segment.html#ga059e07ca7a0f613dadbbaff777df369a", null ],
    [ "LIB_7S_display_assembly_t", "d9/d68/group__seven__segment.html#ga0122f7bc2ee3b2c837c58efd871fe8f8", null ],
    [ "LIB_7S_format_t", "d9/d68/group__seven__segment.html#gacc7a14fe6025f5830a024dd90b53dd84", null ],
    [ "LIB_7S_LED_configuration_t", "d9/d68/group__seven__segment.html#gad802fe84fa3ce80b0e91be712ea33ea7", null ],
    [ "LIB_7S_segment_bit_position_t", "d9/d68/group__seven__segment.html#ga7f6ce812b24d01b9f919f550a9ad73e4", null ],
    [ "LIB_7S_segment_portpin_t", "d9/d68/group__seven__segment.html#ga35b91ca1a026dbe9dfc0dc1a16b7f8de", null ],
    [ "LIB_7S_SPI_communication_t", "d9/d68/group__seven__segment.html#ga13c4420a092a4b0828400039e9f6b9d0", null ],
    [ "LIB_alignment_t", "d9/d68/group__seven__segment.html#ga7f8534ba3c2b6138f8417ef8e95e97a0", null ],
    [ "LIB_7S_alignment_e", "d9/d68/group__seven__segment.html#ga02efcd0417189e9b7f4e6a892fe108a4", [
      [ "RIGHT", "d9/d68/group__seven__segment.html#gga02efcd0417189e9b7f4e6a892fe108a4aec8379af7490bb9eaaf579cf17876f38", null ],
      [ "CENTER", "d9/d68/group__seven__segment.html#gga02efcd0417189e9b7f4e6a892fe108a4a2159ffbd3a68037511ab5ab4dd35ace7", null ],
      [ "LEFT", "d9/d68/group__seven__segment.html#gga02efcd0417189e9b7f4e6a892fe108a4adb45120aafd37a973140edee24708065", null ]
    ] ],
    [ "LIB_7S_LED_configuration_e", "d9/d68/group__seven__segment.html#gabd3822f6e41663fb76fcec1daa9d4b70", [
      [ "COMMON_ANODE", "d9/d68/group__seven__segment.html#ggabd3822f6e41663fb76fcec1daa9d4b70a7153b76a77f2580d4b6882e968e29d8a", null ],
      [ "COMMON_CATHODE", "d9/d68/group__seven__segment.html#ggabd3822f6e41663fb76fcec1daa9d4b70a20d1740395cf478724a726d9d8859fb6", null ]
    ] ],
    [ "LIB_7S_digit_increment", "d9/d68/group__seven__segment.html#ga93505069cb0d0c2f2d0a8a5a026b9401", null ],
    [ "LIB_7S_GPIO_clear", "d9/d68/group__seven__segment.html#ga7b5f5b895629ef6248b63d5082b99956", null ],
    [ "LIB_7S_GPIO_show_char", "d9/d68/group__seven__segment.html#gaf361ef99785cf44ae706c5a983dcbc8b", null ],
    [ "LIB_7S_set_brightness", "d9/d68/group__seven__segment.html#ga80c980a442f078a47cf3e267cf3ae794", null ],
    [ "LIB_7S_set_digit_portpin", "d9/d68/group__seven__segment.html#ga1fc766431c6a4b8a7d6060dd1b1277ee", null ],
    [ "LIB_7S_set_display_assembly", "d9/d68/group__seven__segment.html#ga37cc1770dc0d736542127c069352c4c8", null ],
    [ "LIB_7S_set_format", "d9/d68/group__seven__segment.html#ga7a3acc86a9c81b456dbab1c5e6caed8e", null ],
    [ "LIB_7S_set_segment_bit_position", "d9/d68/group__seven__segment.html#ga0b0df12c067892deb35f2cd7e9f1499b", null ],
    [ "LIB_7S_set_segment_portpin", "d9/d68/group__seven__segment.html#ga562feebb6d74c5dae794a2973b91df52", null ],
    [ "LIB_7S_SPI_clear", "d9/d68/group__seven__segment.html#ga9cbea8c4366e946375178572723b4a6f", null ],
    [ "LIB_7S_SPI_DMA_TIM_show_number", "d9/d68/group__seven__segment.html#ga912075654627d5a8bc640eef908039c7", null ],
    [ "LIB_7S_SPI_DMA_TIM_start_continious_tx", "d9/d68/group__seven__segment.html#ga714f371219ce67947f2eaa45022770bc", null ],
    [ "LIB_7S_SPI_DMA_TIM_stop_continious_tx", "d9/d68/group__seven__segment.html#ga0c72e72fde919e87dee9226336d30ffd", null ],
    [ "LIB_7S_SPI_init", "d9/d68/group__seven__segment.html#ga289ec3880543ae0ddbac65b83abb14f3", null ],
    [ "characters", "d9/d68/group__seven__segment.html#gaf269d2cb5f5c87c6d3af1031f29e7294", null ]
];