var group___timer =
[
    [ "Timer_Exported_Constants", "d8/df6/group___timer___exported___constants.html", null ],
    [ "Timer_Private_Functions", "d8/dbd/group___timer___private___functions.html", "d8/dbd/group___timer___private___functions" ],
    [ "API_TIM_type_t", "da/de0/struct_a_p_i___t_i_m__type__t.html", [
      [ "DMAx_Channelx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a15ccb5efead399be16dcc79e6ef0afb4", null ],
      [ "GPIO_AF_TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a53ab4d29115009c0c013b4bb100c5655", null ],
      [ "IRQnx", "da/de0/struct_a_p_i___t_i_m__type__t.html#aae957cbdbce5c44eac6cf46d4a099064", null ],
      [ "period_timer_ticks", "da/de0/struct_a_p_i___t_i_m__type__t.html#abd297952e4487813eb634ecca54b2b0a", null ],
      [ "RCC_APBxPeriph_TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#abb91f6582261e0f29fc7c7e21fe1e4bc", null ],
      [ "TIMx", "da/de0/struct_a_p_i___t_i_m__type__t.html#a663322e9eb0b01f9723211f3973c31ef", null ]
    ] ],
    [ "API_PWM_set_dutycycle", "dc/d7b/group___timer.html#ga7165fe021c9e7b3e8d6bf18873df1880", null ],
    [ "API_PWM_start", "dc/d7b/group___timer.html#gacf893466ef0883963ced073b0807240b", null ],
    [ "API_TIM_start", "dc/d7b/group___timer.html#gabc8c05da4b67c2963e4fa5bbad20c952", null ],
    [ "API_TIM_stop", "dc/d7b/group___timer.html#ga78bf85bb15b023b7977a06544f5e1a3c", null ]
];